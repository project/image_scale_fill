# Image Scale and Fill Background

The Image Scale and Fill Background module provides a Drupal image effect plugin that scales an image to fit a specified canvas size and fills any additional space with a stylistic background. This can be either a blurred version of the image or its dominant color. This functionality allows users to maintain the aspect ratio of images without cropping content, ideal for responsive designs where image dimensions vary.

## Features

- **Scale to Fit**: Dynamically scales images to fit within a specified width and height.
- **Background Fill**: Fills excess space with a choice of:
  - A blurred version of the image.
  - The dominant color extracted from the image.
  - A gradient based on the dominant color.
- **Customizable Options**: Configurable options for blur intensity, gradient direction, and resize ratio.

## Configuration

The image effect can be configured with the following options:

- **Width and Height**: Define the dimensions of the target area the image should scale to fit.
- **Background Type**: Choose between a blurred background, dominant color, or gradient fill.
- **Blur Radius and Sigma**: Specify the intensity and spread of the blur effect (applicable if the blurred background type is selected).
- **Resize Ratio**: Set the scale percentage to resize the image before applying effects to optimize performance and create the right effect you are looking for.
- **Gradient Direction**: Choose the direction of the gradient fill, either vertical or horizontal (if gradient background type is selected).

## Usage

1. Navigate to the Image Styles configuration page (`/admin/config/media/image-styles`).
2. Add a new style or edit an existing one.
3. Add the `Scale and Fill Background` effect from the list of available effects.
4. Configure the effect according to your needs.
5. Apply the style to image fields as required.

## Dependencies

- Drupal Core
- Image module (included in Drupal Core)

This module uses the GaussianBlur code from image_effect module for the blur effect. No external libraries are required other than php-gd.

## License

This project is GPL-2.0-or-later software. See the LICENSE file in the source directory for complete text.

## Maintainers

Current maintainers:
- [vimal Joseph](https://www.drupal.org/u/vimaljoseph)

## Supporting organization
- [Zyxware Technologies](https://www.zyxware.com)

Feel free to report any issues or feature requests in the Drupal issue queue for this project.

