<?php

namespace Drupal\image_scale_fill\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;
use Drupal\image_scale_fill\Component\GdGaussianBlur;

/**
 * Provides a 'Scale and Fill Background Effect' for Image Styles.
 *
 * @ImageEffect(
 *   id = "scale_and_fill_background",
 *   label = @Translation("Scale and Fill Background"),
 *   description = @Translation("Scales an image to fit a specified area and fills the rest with a background effect like a blurred image or dominant color.")
 * )
 *
 * @ingroup image
 */
class ScaleFillBackgroundEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    $width = $this->configuration['width'];
    $height = $this->configuration['height'];
    $background_type = $this->configuration['background_type'];
    $background_style = $this->configuration['background_style'];
    $gradient_direction = $this->configuration['gradient_direction'];

    $background = imagecreatetruecolor($width, $height);

    switch ($background_type) {
      case 'tiled':
        $this->applyTiledBackground($image, $background);
        break;

      case 'scaled':
        $this->applyScaledBackground($image, $background);
        break;

      case 'dominant_color':
        if ($background_style == 'gradient') {
          $this->applyGradientBackground($image, $background, $gradient_direction);
        }
        else {
          $this->applyDominantColorBackground($image, $background);
        }
        break;
    }

    if ($background_style == 'blurred') {
      $this->applyBlurredBackground($background);
    }
    elseif ($background_style == 'transparent_overlay') {
      $this->applyTransparentOverlayBackground($background);
    }

    // Common final steps to place the original image centered on the background.
    $this->placeCenterImage($image, $background);

    // Set the modified background as the final image resource.
    $image->getToolkit()->setImage($background);
    return TRUE;
  }

  /**
   * Applies a blurred background.
   *
   * @internal
   *
   * @param resource $background
   *   The background image resource.
   */
  private function applyBlurredBackground($background) {
    $original_resource = $background;
    $original_width = imagesx($original_resource);
    $original_height = imagesy($original_resource);

    $resize_ratio = $this->configuration['resize_ratio'];
    $scaled_width = round($original_width * $resize_ratio);
    $scaled_height = round($original_height * $resize_ratio);

    $tiny_image = imagecreatetruecolor($scaled_width, $scaled_height);
    imagecopyresampled($tiny_image, $original_resource, 0, 0, 0, 0, $scaled_width, $scaled_height, $original_width, $original_height);

    $radius = $this->configuration['radius'];
    $sigma = $this->configuration['sigma'] ?? $radius * 2 / 3;
    $coeffs = GdGaussianBlur::gaussianCoeffs($radius, $sigma);
    GdGaussianBlur::applyCoeffs($tiny_image, $tiny_image, $coeffs, $radius, 'HORIZONTAL');
    GdGaussianBlur::applyCoeffs($tiny_image, $tiny_image, $coeffs, $radius, 'VERTICAL');

    $canvas_width = $this->configuration['width'];
    $canvas_height = $this->configuration['height'];
    $aspect_ratio_original = $original_width / $original_height;
    $aspect_ratio_canvas = $canvas_width / $canvas_height;

    if ($aspect_ratio_original > $aspect_ratio_canvas) {
      $final_width = round($scaled_width * ($canvas_height / $scaled_height));
      $final_height = $canvas_height;
    }
    else {
      $final_width = $canvas_width;
      $final_height = round($scaled_height * ($canvas_width / $scaled_width));
    }

    $offset_x = round(max(0, ($final_width - $canvas_width) / 2));
    $offset_y = round(max(0, ($final_height - $canvas_height) / 2));

    imagecopyresampled($background, $tiny_image, -$offset_x, -$offset_y, 0, 0, $final_width, $final_height, $scaled_width, $scaled_height);

    imagedestroy($tiny_image);
  }

  /**
   * Applies a transparent overlay background.
   */
  private function applyTransparentOverlayBackground($background) {
    $canvas_width = $this->configuration['width'];
    $canvas_height = $this->configuration['height'];

    $overlay = imagecreatetruecolor($canvas_width, $canvas_height);
    $transparency = $this->configuration['transparency'];
    $alpha_value = round(127 * (100 - $transparency) / 100);
    $transparent_color = imagecolorallocatealpha($overlay, 0, 0, 0, $alpha_value);
    imagefill($overlay, 0, 0, $transparent_color);

    imagecopymerge($background, $overlay, 0, 0, 0, 0, $canvas_width, $canvas_height, $transparency);

    imagedestroy($overlay);
  }

  /**
   * Applies a tiled background.
   */
  private function applyTiledBackground(ImageInterface $image, $background) {
    $original_resource = $image->getToolkit()->getImage();
    $original_width = imagesx($original_resource);
    $original_height = imagesy($original_resource);

    // Canvas dimensions.
    $background_width = $this->configuration['width'];
    $background_height = $this->configuration['height'];
    $aspect_ratio_original = $original_width / $original_height;
    $aspect_ratio_background = $background_width / $background_height;

    // Determine scaling factor and new dimensions to fit the original image within the canvas.
    if ($aspect_ratio_original > $aspect_ratio_background) {
      // Landscape: scale based on width.
      $scaled_width = $background_width;
      $scaled_height = intval($original_height * ($background_width / $original_width));
    }
    else {
      // Portrait: scale based on height.
      $scaled_width = intval($original_width * ($background_height / $original_height));
      $scaled_height = $background_height;
    }

    // Create a black block of the scaled image size in the center.
    $center_x = intval(($background_width - $scaled_width) / 2);
    $center_y = intval(($background_height - $scaled_height) / 2);
    $black = imagecolorallocate($background, 0, 0, 0);
    imagefilledrectangle($background, $center_x, $center_y, $center_x + $scaled_width, $center_y + $scaled_height, $black);

    // Resize the original image to the new dimensions.
    $resized_image = imagecreatetruecolor($scaled_width, $scaled_height);
    imagecopyresampled($resized_image, $original_resource, 0, 0, 0, 0, $scaled_width, $scaled_height, $original_width, $original_height);

    // Fill the excess space symmetrically using parts of the resized image.
    $excess_height = $background_height - $scaled_height;
    $excess_width = $background_width - $scaled_width;

    // Fill top and bottom.
    if ($excess_height > 0) {
      for ($y = 0; $y < $center_y; $y += $scaled_height) {
        $remaining_height = min($scaled_height, $center_y - $y);
        imagecopy($background, $resized_image, 0, $y, 0, 0, $scaled_width, $remaining_height);
        imagecopy($background, $resized_image, 0, $background_height - $y - $remaining_height, 0, $scaled_height - $remaining_height, $scaled_width, $remaining_height);
      }
    }

    // Fill left and right.
    if ($excess_width > 0) {
      for ($x = 0; $x < $center_x; $x += $scaled_width) {
        $remaining_width = min($scaled_width, $center_x - $x);
        imagecopy($background, $resized_image, $x, 0, 0, 0, $remaining_width, $scaled_height);
        imagecopy($background, $resized_image, $background_width - $x - $remaining_width, 0, $scaled_width - $remaining_width, 0, $remaining_width, $scaled_height);
      }
    }

  }

  /**
   * Applies a scaled background.
   */
  private function applyScaledBackground(ImageInterface $image, $background) {
    $original_resource = $image->getToolkit()->getImage();
    $original_width = imagesx($original_resource);
    $original_height = imagesy($original_resource);

    $canvas_width = $this->configuration['width'];
    $canvas_height = $this->configuration['height'];
    $aspect_ratio_original = $original_width / $original_height;
    $aspect_ratio_canvas = $canvas_width / $canvas_height;

    if ($aspect_ratio_original > $aspect_ratio_canvas) {
      $final_width = (int) ($original_width * ($canvas_height / $original_height));
      $final_height = $canvas_height;
    }
    else {
      $final_width = $canvas_width;
      $final_height = (int) ($original_height * ($canvas_width / $original_width));
    }

    $offset_x = max(0, ($final_width - $canvas_width) / 2);
    $offset_y = max(0, ($final_height - $canvas_height) / 2);

    imagecopyresampled($background, $original_resource, -$offset_x, -$offset_y, 0, 0, $final_width, $final_height, $original_width, $original_height);
  }

  /**
   * Applies a background with the dominant color.
   *
   * @internal
   *
   * @param \Drupal\Core\Image\ImageInterface $image
   *   The image object.
   * @param resource $background
   *   The background image resource.
   */
  private function applyDominantColorBackground(ImageInterface $image, $background) {
    $dominantColor = $this->getDominantColor($image);
    $color = imagecolorallocate($background, $dominantColor['red'], $dominantColor['green'], $dominantColor['blue']);
    imagefill($background, 0, 0, $color);
  }

  /**
   * Applies a gradient background.
   *
   * @internal
   *
   * @param \Drupal\Core\Image\ImageInterface $image
   *   The image object.
   * @param resource $background
   *   The background image resource.
   * @param string $direction
   *   The gradient direction ('vertical' or 'horizontal').
   */
  private function applyGradientBackground(ImageInterface $image, $background, $direction) {
    $dominantColor = $this->getDominantColor($image);
    $startColor = imagecolorallocate($background, $dominantColor['red'], $dominantColor['green'], $dominantColor['blue']);
    $endColor = imagecolorallocate($background, max(0, $dominantColor['red'] - 50), max(0, $dominantColor['green'] - 50), max(0, $dominantColor['blue'] - 50));

    $this->applyGradient($background, $startColor, $endColor, $direction, $this->configuration['width'], $this->configuration['height']);
  }

  /**
   * Helper function to interpolate and apply gradient.
   *
   * @internal
   *
   * @param resource $image
   *   The image resource.
   * @param int $startColor
   *   The starting color.
   * @param int $endColor
   *   The ending color.
   * @param string $direction
   *   The gradient direction.
   * @param int $width
   *   The width of the image.
   * @param int $height
   *   The height of the image.
   */
  private function applyGradient($image, $startColor, $endColor, $direction, $width, $height) {
    if ($direction === 'vertical') {
      for ($y = 0; $y < $height; $y++) {
        $interpolatedColor = $this->interpolateColor($image, $startColor, $endColor, $y / $height);
        imageline($image, 0, $y, $width, $y, $interpolatedColor);
      }
    }
    else {
      for ($x = 0; $x < $width; $x++) {
        $interpolatedColor = $this->interpolateColor($image, $startColor, $endColor, $x / $width);
        imageline($image, $x, 0, $x, $height, $interpolatedColor);
      }
    }
  }

  /**
   * Interpolates colors for gradients.
   *
   * @internal
   *
   * @param resource $image
   *   The image resource.
   * @param int $startColor
   *   The starting color.
   * @param int $endColor
   *   The ending color.
   * @param float $factor
   *   The interpolation factor.
   *
   * @return int
   *   The interpolated color.
   */
  private function interpolateColor($image, $startColor, $endColor, $factor) {
    $start_red = ($startColor >> 16) & 0xFF;
    $start_green = ($startColor >> 8) & 0xFF;
    $start_blue = $startColor & 0xFF;

    $end_red = ($endColor >> 16) & 0xFF;
    $end_green = ($endColor >> 8) & 0xFF;
    $end_blue = $endColor & 0xFF;

    $red = (int) ($start_red + ($factor * ($end_red - $start_red)));
    $green = (int) ($start_green + ($factor * ($end_green - $start_green)));
    $blue = (int) ($start_blue + ($factor * ($end_blue - $start_blue)));

    return imagecolorallocate($image, $red, $green, $blue);
  }

  /**
   * Extracts the most dominant color from the image.
   *
   * @internal
   *
   * @param \Drupal\Core\Image\ImageInterface $image
   *   The image object.
   *
   * @return array
   *   An array containing 'red', 'green', and 'blue' values.
   */
  private function getDominantColor(ImageInterface $image) {
    $size = 100;
    $resized = imagecreatetruecolor($size, $size);
    imagecopyresampled($resized, $image->getToolkit()->getImage(), 0, 0, 0, 0, $size, $size, $image->getWidth(), $image->getHeight());

    $colors = [];
    for ($x = 0; $x < $size; $x++) {
      for ($y = 0; $y < $size; $y++) {
        $rgb = imagecolorat($resized, $x, $y);
        $colors[] = $rgb;
      }
    }
    imagedestroy($resized);

    $colors = array_count_values($colors);
    arsort($colors);
    $mostFrequent = key($colors);

    return [
      'red' => ($mostFrequent >> 16) & 0xFF,
      'green' => ($mostFrequent >> 8) & 0xFF,
      'blue' => $mostFrequent & 0xFF,
    ];
  }

  /**
   * Places the original image centered over the background.
   */
  private function placeCenterImage(ImageInterface $image, $background) {
    $original_width = $image->getWidth();
    $original_height = $image->getHeight();
    $canvas_width = $this->configuration['width'];
    $canvas_height = $this->configuration['height'];

    // Calculate dimensions to fit within the canvas while maintaining the aspect ratio.
    $scale = min($canvas_width / $original_width, $canvas_height / $original_height);
    $center_width = round($original_width * $scale);
    $center_height = round($original_height * $scale);

    // Create a new image with the calculated dimensions.
    $center_image = imagecreatetruecolor($center_width, $center_height);
    imagecopyresampled($center_image, $image->getToolkit()->getImage(), 0, 0, 0, 0, $center_width, $center_height, $original_width, $original_height);

    // Calculate positions to center the image on the canvas.
    $center_x = round(($canvas_width - $center_width) / 2);
    $center_y = round(($canvas_height - $center_height) / 2);

    // Place the centered image on the background.
    imagecopy($background, $center_image, $center_x, $center_y, 0, 0, $center_width, $center_height);

    // Clean up resources.
    imagedestroy($center_image);
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = [
      '#markup' => $this->t('Scales and fills the background of an image according to specified settings.') . '<br>' .
      $this->t('Width: @width px', ['@width' => $this->configuration['width']]) . '<br>' .
      $this->t('Height: @height px', ['@height' => $this->configuration['height']]) . '<br>' .
      $this->t('Background Type: @type', ['@type' => $this->configuration['background_type']]),
    ];

    switch ($this->configuration['background_type']) {
      case 'tiled':
      case 'scaled':
        $summary['#markup'] .= '<br>' . $this->t('Background Style: @style', ['@style' => $this->configuration['background_style']]);
        break;

      case 'dominant_color':
        $summary['#markup'] .= '<br>' . $this->t('Background Style: @style', ['@style' => $this->configuration['background_style']])
          . '<br>' . $this->t('Gradient Direction: @direction', ['@direction' => $this->configuration['gradient_direction']]);
        break;
    }

    if ($this->configuration['background_style'] == 'blurred') {
      $summary['#markup'] .= '<br>' . $this->t('Blur Radius: @radius', ['@radius' => $this->configuration['radius']])
        . '<br>' . $this->t('Blur Sigma: @sigma', ['@sigma' => $this->configuration['sigma']])
        . '<br>' . $this->t('Resize Ratio: @resize_ratio', ['@resize_ratio' => $this->configuration['resize_ratio']]);
    }
    elseif ($this->configuration['background_style'] == 'transparent_overlay') {
      $summary['#markup'] .= '<br>' . $this->t('Transparency: @transparency%', ['@transparency' => $this->configuration['transparency']]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'width' => '',
      'height' => '',
      'background_type' => 'scaled',
      'background_style' => 'blurred',
      'background_style_dominant' => 'single_color',
      'radius' => 9,
      'sigma' => 6,
      'resize_ratio' => 0.3,
      'gradient_direction' => 'vertical',
      'transparency' => 60,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->configuration['width'],
      '#required' => TRUE,
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->configuration['height'],
      '#required' => TRUE,
    ];

    $form['background_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Background Type'),
      '#options' => [
        'tiled' => $this->t('Tiled'),
        'scaled' => $this->t('Scaled'),
        'dominant_color' => $this->t('Dominant Color'),
      ],
      '#default_value' => $this->configuration['background_type'],
    ];

    $form['background_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Background Style'),
      '#options' => [
        'blurred' => $this->t('Blurred'),
        'transparent_overlay' => $this->t('Coloured Overlay'),
      ],
      '#default_value' => $this->configuration['background_style'],
      '#states' => [
        'visible' => [
          ':input[name="data[background_type]"]' => [
            ['value' => 'tiled'],
            ['value' => 'scaled'],
          ],
        ],
      ],
    ];

    $form['background_style_dominant'] = [
      '#type' => 'select',
      '#title' => $this->t('Background Style'),
      '#options' => [
        'single_color' => $this->t('Single Color'),
        'gradient' => $this->t('Gradient'),
      ],
      '#default_value' => $this->configuration['background_style_dominant'],
      '#states' => [
        'visible' => [
          ':input[name="data[background_type]"]' => ['value' => 'dominant_color'],
        ],
      ],
    ];

    $form['radius'] = [
      '#type' => 'number',
      '#title' => $this->t('Blur Radius'),
      '#description' => $this->t('Specifies the radius of the blur effect. Larger values can take more time to process images'),
      '#default_value' => $this->configuration['radius'],
      '#min' => 1,
      '#max' => 50,
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="data[background_style]"]' => ['value' => 'blurred'],
        ],
      ],
    ];

    $form['sigma'] = [
      '#type' => 'number',
      '#title' => $this->t('Blur Sigma'),
      '#description' => $this->t('Determines the spread of the blur effect. Larger values can take more time to process images'),
      '#default_value' => $this->configuration['sigma'],
      '#min' => 0.1,
      '#max' => 50,
      '#step' => 0.1,
      '#states' => [
        'visible' => [
          ':input[name="data[background_style]"]' => ['value' => 'blurred'],
        ],
      ],
    ];

    $form['resize_ratio'] = [
      '#type' => 'number',
      '#title' => $this->t('Resize Ratio'),
      '#description' => $this->t('Percentage to reduce the image before applying background effects. Larger values can take more time to process images'),
      '#default_value' => $this->configuration['resize_ratio'],
      '#min' => 0.1,
      '#max' => 1,
      '#step' => 0.1,
      '#states' => [
        'visible' => [
          ':input[name="data[background_style]"]' => ['value' => 'blurred'],
        ],
      ],
    ];

    $form['gradient_direction'] = [
      '#type' => 'select',
      '#title' => $this->t('Gradient Direction'),
      '#options' => [
        'vertical' => $this->t('Vertical'),
        'horizontal' => $this->t('Horizontal'),
      ],
      '#default_value' => $this->configuration['gradient_direction'],
      '#states' => [
        'visible' => [
          ':input[name="data[background_style_dominant]"]' => ['value' => 'gradient'],
        ],
      ],
    ];

    $form['transparency'] = [
      '#type' => 'number',
      '#title' => $this->t('Overlay Transparency'),
      '#description' => $this->t('Set the transparency level for the overlay. 0 is fully transparent, and 100 is fully opaque.'),
      '#default_value' => $this->configuration['transparency'],
      '#min' => 0,
      '#max' => 100,
      '#states' => [
        'visible' => [
          ':input[name="data[background_style]"]' => ['value' => 'transparent_overlay'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['width'] = $form_state->getValue('width');
    $this->configuration['height'] = $form_state->getValue('height');
    $this->configuration['background_type'] = $form_state->getValue('background_type');
    $this->configuration['background_style'] = $form_state->getValue('background_style');
    $this->configuration['background_style_dominent'] = $form_state->getValue('background_style');
    $this->configuration['radius'] = $form_state->getValue('radius');
    $this->configuration['sigma'] = $form_state->getValue('sigma');
    $this->configuration['resize_ratio'] = $form_state->getValue('resize_ratio');
    $this->configuration['gradient_direction'] = $form_state->getValue('gradient_direction');
    $this->configuration['transparency'] = $form_state->getValue('transparency');
  }

  /**
   * {@inheritdoc}
   */
  public function transformDimensions(array &$dimensions, $uri) {
    if ($dimensions['width'] && $dimensions['height']) {
      // Set the exact dimensions since we're filling to a specific canvas size
      $dimensions['width'] = $this->configuration['width'];
      $dimensions['height'] = $this->configuration['height'];
    }
  }

}
